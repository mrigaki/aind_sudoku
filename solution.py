assignments = []


def assign_value(values, box, value):
    """
    Please use this function to update your values dictionary!
    Assigns a value to a given box. If it updates the board record it.
    """
    values[box] = value
    if len(value) == 1:
        assignments.append(values.copy())
    return values


def naked_twins(values):
    """Eliminate values using the naked twins strategy.
    Args:
        values(dict): a dictionary of the form {'box_name': '123456789', ...}

    Returns:
        the values dictionary with the naked twins eliminated from peers.
    """
    # Find all instances of naked twins
    for k, v in values.items():
        if len(v) == 2:
            for unit in units[k]:
                for u in unit:
                    if (values[u] == v) and (u != k):
                        # Eliminate the naked twins as possibilities for the respective unit
                        values = eliminate_twin(k, u, unit, values)

    return values


def eliminate_twin(box1, box2, unit, values):
    """ Remove the digits in the twin from the other boxes in the unit """
    twins = values[box1]
    for b in unit:
        if (b != box1) and (b != box2):
            for twin in twins:
                values = assign_value(values, b, values[b].replace(twin, ''))
    return values


def cross(A, B):
    """Cross product of elements in A and elements in B."""
    return [s + t for s in A for t in B]


def grid_values(grid):
    """
    Convert grid into a dict of {square: char} with '123456789' for empties.
    Args:
        grid(string) - A grid in string form.
    Returns:
        A grid in dictionary form
            Keys: The boxes, e.g., 'A1'
            Values: The value in each box, e.g., '8'. If the box has no value, then the value will be '123456789'.
    """
    chars = []
    digits = '123456789'
    for c in grid:
        if c == '.':
            chars.append(digits)
        else:
            chars.append(c)
    assert len(chars) == 81
    return dict(zip(boxes, chars))


def display(values):
    """
    Display the values as a 2-D grid.
    Args:
        values(dict): The sudoku in dictionary form
    """
    width = 1 + max(len(values[s]) for s in boxes)
    line = '+'.join(['-' * (width * 3)] * 3)
    for r in rows:
        print(''.join(values[r + c].center(width) + ('|' if c in '36' else '')
                      for c in cols))
    if r in 'CF':
        print(line)
    return


def eliminate(values):
    """
    Eliminate a box's single value from its peers
    Args:
        values(dict): The sudoku in dictionary form
    Returns:
        values(dict): The sudoku in dictionary form after elimination
    """
    solved_values = [box for box in values.keys() if len(values[box]) == 1]
    for box in solved_values:
        digit = values[box]
        for peer in peers[box]:
            values = assign_value(values, peer, values[peer].replace(digit, ''))
    return values


def only_choice(values):
    """
    Apply the Only Choice constraint
    Args:
        values(dict): The sudoku in dictionary form
    Returns:
        values(dict): The sudoku in dictionary form after applying the Only Choice constraint
    """
    for unit in unitlist:
        for digit in '123456789':
            dplaces = [box for box in unit if digit in values[box]]
            if len(dplaces) == 1:
                values = assign_value(values, dplaces[0], digit)
    return values


def reduce_puzzle(values):
    """
    Try to solve the puzzle by applying the constraints
    Args:
        values(dict): The sudoku in dictionary form
    Returns:
        values(dict): The sudoku in dictionary form after the elimination
        or False if the reduction is stalled
    """
    # solved_values = [box for box in values.keys() if len(values[box]) == 1]
    stalled = False
    while not stalled:
        solved_values_before = len([box for box in values.keys() if len(values[box]) == 1])
        values = eliminate(values)
        values = only_choice(values)
        # values = naked_twins(values)
        solved_values_after = len([box for box in values.keys() if len(values[box]) == 1])
        stalled = solved_values_before == solved_values_after
        if len([box for box in values.keys() if len(values[box]) == 0]):
            return False
    return values


def solve(grid):
    return search(grid_values(grid))


def search(values):
    """
    Perform DFS search
    Args:
        values(dict): The sudoku in dictionary form
    Returns:
        values(dict): The sudoku solution
        or False for a failed effort
    """
    values = reduce_puzzle(values)
    # print("After #################")
    # display(values)
    if values is False:
        return False  ## Failed earlier

    if all(len(values[s]) == 1 for s in boxes):
        return values  ## Solved!

    # Chose one of the unfilled square s with the fewest possibilities
    n, s = min((len(values[s]), s) for s in boxes if len(values[s]) > 1)

    # Now use recurrence to solve each one of the resulting sudokus, and
    for value in values[s]:
        new_sudoku = values.copy()
        new_sudoku = assign_value(new_sudoku, s, value)
        attempt = search(new_sudoku)
        if attempt:
            return attempt

    return values


rows = 'ABCDEFGHI'
cols = '123456789'

boxes = cross(rows, cols)

row_units = [cross(r, cols) for r in rows]
column_units = [cross(rows, c) for c in cols]
square_units = [cross(rs, cs) for rs in ('ABC', 'DEF', 'GHI') for cs in ('123', '456', '789')]

# Add diagonal units in the unit list
diag1_unit = [''.join(list(i)) for i in zip(rows, cols)]
diag2_unit = [''.join(list(i)) for i in zip(rows[::-1], cols)]
diags = [diag1_unit, diag2_unit]

unitlist = row_units + column_units + square_units + diags
units = dict((s, [u for u in unitlist if s in u]) for s in boxes)
peers = dict((s, set(sum(units[s], [])) - set([s])) for s in boxes)

if __name__ == '__main__':
    diag_sudoku_grid = '2.............62....1....7...6..8...3...9...7...6..4...4....8....52.............3'
    display(solve(diag_sudoku_grid))

    try:
        from visualize import visualize_assignments
        visualize_assignments(assignments)
    except:
        print('We could not visualize your board due to a pygame issue. Not a problem! It is not a requirement.')
